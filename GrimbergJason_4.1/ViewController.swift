//
//  ViewController.swift
//  GrimbergJason_4.1
//
//  Created by Jason Grimberg on 10/29/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer = Timer()
    var timerIsOn = false
    var timeRemaining = 5
    var runCount = 0
    var currentTime = ""
    
    @IBOutlet weak var playButtonInteraction: UIButton!
    
    @IBOutlet weak var missedLabel: UILabel!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var collectionView2: UICollectionView!
    
    @IBAction func play(_ sender: UIButton) {
        runCount = 0
        timerIsOn = false
        timeRemaining = 5
        timeLabel.text = "00:05"
        missedLabel.text = "Missed: \(score)"
        timer.invalidate()
        playButtonInteraction.setTitle("Reset", for: .normal)
        
        if !timerIsOn {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self,
                selector: #selector(timerRunning), userInfo: nil, repeats: true)
            // Keep the timer on until 0
            timerIsOn = true
        }
        
        start()
        setup()
        
        playButtonInteraction.isUserInteractionEnabled = false
    }
    
    private var deck = [String]()
    private var selectedIndexes = Array<IndexPath>()
    private var numberOfPairs = 0
    private var score = 0
    private var newGame = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Check what device we are using
        if UIDevice.current.userInterfaceIdiom == .pad {
            collectionView = collectionView2
            // Set the .tag based on if we are using an ipad or not
            collectionView.tag = 1
        } else {
            // Not an iPad
            collectionView.tag = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: The Puzzle layout class
class PuzzleViewLayout: UICollectionViewLayout {
    
    var columns: Int = 0
    var padding: CGFloat = 3.0
    
    // Collection of all attributes
    var layoutAttributes = [UICollectionViewLayoutAttributes]()
    // Size of the content
    var contentHeight: CGFloat = 0.0
    var contentWidth: CGFloat = 0.0
    
    // Prepare function
    override func prepare() {
        if collectionView?.tag != 1 {
            columns = 4
        } else {
            columns = 5
        }
        
        let insets = collectionView!.contentInset
        self.contentWidth = collectionView!.bounds.width - (insets.left + insets.right)
        let columnWidth = self.contentWidth / CGFloat(columns)
        
        var column = 0
        
        // Vertical offset
        var topOffset = [CGFloat](repeating: 0, count: columns)
        
        // Horizontal offset
        var offset = [CGFloat]()
        
        for column in 0 ..< columns {
            offset += [CGFloat(column) * columnWidth]
        }
        
        // Consider only th first section
        let section = 0
        
        for item in 0 ..< collectionView!.numberOfItems(inSection: section) {
            let indexPath = IndexPath(row: item, section: section)
            
            // Pick the height of each cell
            var height:CGFloat = 0
            
            if collectionView?.tag != 1 {
                height = 64
            } else {
                height = 128
            }
            
            // Use the precalculated values from the previous items
            let frame = CGRect(x: offset[column], y: topOffset[column], width: columnWidth, height: height)
            
            let insetFrame = frame.insetBy(dx: padding, dy: padding)
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            
            self.layoutAttributes.append(attributes)
            
            // Stretch the content view bounds
            self.contentHeight = max(frame.maxY, contentHeight)
            
            // Move to the next y position
            topOffset[column] = topOffset[column] + height
            
            // Move to the next column and always stay in valid index [0 ..columns - 1]
            column = (column + 1) % columns
        }
    }
    
    
    // Return size of the whole view collection
    override var collectionViewContentSize : CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var attrs = [UICollectionViewLayoutAttributes]()
        
        // Send all items which are visible in the current rectangle
        for itemAttributes in self.layoutAttributes {
            if itemAttributes.frame.intersects(rect) {
                attrs.append(itemAttributes)
            }
        }
        return attrs
    }
}

// MARK: UICollectionViewDataSource
extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deck.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cardCell", for: indexPath) as! CardCell
        
        let card = deck[indexPath.row]
        cell.renderCardName(cardImageName: card.description, backImageName: "back")
        
        if newGame == 0 {
            // Turn off the user interaction for the preview
            collectionView.isUserInteractionEnabled = false
            // Preview all of the cards
            cell.upturn()
            // Append all of the cells so we can preview them
            selectedIndexes.append(indexPath)
            // Flip all of the cards back over after 5 seconds
            perform(#selector(turnCardsFaceDown), with: nil, afterDelay: 5)
        }
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension ViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if selectedIndexes.count == 2 {
            return
        }
        // Add what was selected
        selectedIndexes.append(indexPath)
        // Turn over the selected cells
        let cell = collectionView.cellForItem(at: indexPath) as! CardCell
        cell.upturn()
        
        if selectedIndexes.count < 2 {
            return
        }
        
        let card1 = deck[selectedIndexes[0].row]
        let card2 = deck[selectedIndexes[1].row]
        
        if card1 == card2 {
            // Add the number of pairs
            numberOfPairs = (numberOfPairs + 1)
            // Check if the game is finished
            checkIfFinished()
            // Remove the selected cards after 1 second
            perform(#selector(removeCards), with: nil, afterDelay: 1)
            
        } else {
            // Add to the score
            score = (score + 1)
            // Update the missed total
            missedLabel.text = "Missed: \(score)"
            // Turn the cards back over with a delay of 1 second
            perform(#selector(turnCardsFaceDown), with: nil, afterDelay: 1)
        }
    }
}

// MARK: Start
private extension ViewController {
    func start() {
        
        deck = [String]()
        
        if collectionView.tag != 1 {
            deck.append("image0")
            deck.append("image1")
            deck.append("image2")
            deck.append("image3")
            deck.append("image4")
            deck.append("image5")
            deck.append("image6")
            deck.append("image7")
            deck.append("image8")
            deck.append("image9")
        } else {
            deck.append("image0")
            deck.append("image1")
            deck.append("image2")
            deck.append("image3")
            deck.append("image4")
            deck.append("image5")
            deck.append("image6")
            deck.append("image7")
            deck.append("image8")
            deck.append("image9")
            deck.append("image10")
            deck.append("image11")
            deck.append("image12")
            deck.append("image13")
            deck.append("image14")
        }
        
        // Start with a zero score
        numberOfPairs = 0
        score = 0
        // Create the deck
        createDeck()
        // Shuffle the deck of images
        shuffled()
        // Reload the collection view data
        collectionView.reloadData()
        // Set the new game
        newGame = 0
        // User interaction for the play button off
        
    }
}

// MARK: Setup
private extension ViewController {
    func setup() {
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isScrollEnabled = false
        collectionView.register(CardCell.self, forCellWithReuseIdentifier: "cardCell")
        collectionView.backgroundColor = UIColor.clear
        
        self.view.addSubview(collectionView)
    }
}

// MARK: Actions
private extension ViewController {
    func checkIfFinished() {
        // Check if the number of pairs is the same as the deck count
        if numberOfPairs == deck.count/2 {
            // Show the final popup
            showFinalPopUp()
        }
    }
    
    // Show the final pop up
    func showFinalPopUp() {
        if score == 0 {
            // Set the alert type
            let alert = UIAlertController(title: "Amazing!",
                message: "You've gotten a perfect score!",
                preferredStyle: UIAlertControllerStyle.alert)
            // Add an Ok button that will dismiss the alert
            alert.addAction(UIAlertAction(title: "Ok",
                style: .default,
                handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
            // Present the alert after completion
            self.present(alert, animated: true, completion: nil)
            timer.invalidate()
        } else {
            
        // Set the alert type
        let alert = UIAlertController(title: "Awesome!",
            message: "You have won!\nTime: \(currentTime) \nYou missed: \(score)",
            preferredStyle: UIAlertControllerStyle.alert)
        // Add an Ok button that will dismiss the alert
        alert.addAction(UIAlertAction(title: "Ok",
            style: .default,
            handler: { action in
                self.dismiss(animated: true, completion: nil)
            }))
        // Present the alert after compleltion
        self.present(alert, animated: true, completion: nil)
            timer.invalidate()
        }
    }
    
    @objc func removeCards() {
        // Remove the cards at the selected indexes
        self.removeCardsAtPlaces(places: self.selectedIndexes)
        // Clear the array after selections were made
        self.selectedIndexes = Array<IndexPath>()
    }
    
    @objc func turnCardsFaceDown() {
        // Turn over the selected cards
        self.downturnCardsAtPlaces(places: self.selectedIndexes)
        // Clear the array after selections were made
        self.selectedIndexes = Array<IndexPath>()
        // Set the user interaction back to true
        collectionView.isUserInteractionEnabled = true
    }
    
    func removeCardsAtPlaces(places: Array<IndexPath>) {
        for index in selectedIndexes {
            let cardCell = collectionView.cellForItem(at: index) as! CardCell
            // Remove the two cells
            cardCell.remove()
        }
    }
    
    func downturnCardsAtPlaces(places: Array<IndexPath>) {
        for index in selectedIndexes {
            let cardCell = collectionView.cellForItem(at: index) as! CardCell
            // Flip the two cards back over
            cardCell.downturn()
        }
    }
    
    func createDeck() {
        // Create a deck with all of our cards and add two of them together
        let halfDeck = deck
        return deck = (halfDeck + halfDeck)
    }
    
    func shuffled(){
        // Temp list w/ both halfs of our deck
        var list = deck
        // Shuffle the temp deck using the old arc4random
        for i in 0..<(list.count - 1) {
            let j = Int(arc4random_uniform(UInt32(list.count - i))) + i
            list.swapAt(i, j)
        }
        // Retrun the shuffled deck
        return deck = list
    }
    
    @objc func timerRunning() {
        // Check if it is a countdown or not
        if timeRemaining != 0 {
            timeRemaining -= 1
            
            let minutesLeft = Int(timeRemaining) / 60 % 60
            let secondsLeft = Int(timeRemaining) % 60
            
            timeLabel.text = "\(String(format: "%02d", minutesLeft)):\(String(format: "%02d", secondsLeft))"
        } else {
            
            playButtonInteraction.isUserInteractionEnabled = true
            
            timerIsOn = true
            runCount += 1
            
            let minutesLeft = Int(runCount) / 60 % 60
            let secondsLeft = Int(runCount) % 60
            
            timeLabel.text = "\(String(format: "%02d", minutesLeft)):\(String(format: "%02d", secondsLeft))"
            currentTime = timeLabel.text!
        }
    }
}


